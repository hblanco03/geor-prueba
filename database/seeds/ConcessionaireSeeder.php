<?php

use Illuminate\Database\Seeder;
use App\Concessionaire;

class ConcessionaireSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Concessionaire::create(
          [
              'name'      => 'concesionario1',
              'email'     => 'concesionario1@gmail.com',
              'ubicacion'  => 'Av El Cuartel',
              'telefono'  => '12345678',
              'locate_id' => '1',
              'status_id' => '1',
          ]);
      Concessionaire::create(
          [
              'name'      => 'concesionario2',
              'email'     => 'concesionario2@gmail.com',
              'ubicacion'  => 'Av universidad',
              'telefono'  => '12345678',
              'locate_id' => '1',
              'status_id' => '1',
          ]);
      Concessionaire::create(
          [
              'name'      => 'concesionario3',
              'email'     => 'concesionario3@gmail.com',
              'ubicacion'  => 'Av Francisco Solano',
              'telefono'  => '12345678',
              'locate_id' => '2',
              'status_id' => '1',
          ]);
      Concessionaire::create(
          [
              'name'      => 'concesionario4',
              'email'     => 'concesionario4@gmail.com',
              'ubicacion'  => 'Av Lecuna',
              'telefono'  => '12345678',
              'locate_id' => '2',
              'status_id' => '1',
          ]);
      Concessionaire::create(
          [
              'name'      => 'concesionario5',
              'email'     => 'concesionario5@gmail.com',
              'ubicacion'  => 'Av Sucre',
              'telefono'  => '12345678',
              'locate_id' => '3',
              'status_id' => '1',
          ]);
      Concessionaire::create(
          [
              'name'      => 'concesionario6',
              'email'     => 'concesionario6@gmail.com',
              'ubicacion'  => 'Av Urdaneta',
              'telefono'  => '12345678',
              'locate_id' => '3',
              'status_id' => '1',
          ]);
      Concessionaire::create(
          [
              'name'      => 'concesionario7',
              'email'     => 'concesionario7@gmail.com',
              'ubicacion'  => 'Av Baral',
              'telefono'  => '12345678',
              'locate_id' => '4',
              'status_id' => '1',
          ]);
      Concessionaire::create(
          [
              'name'      => 'concesionario8',
              'email'     => 'concesionario8@gmail.com',
              'ubicacion'  => 'Av Libertador',
              'telefono'  => '12345678',
              'locate_id' => '4',
              'status_id' => '1',
          ]);

      Concessionaire::create(
          [
              'name'      => 'concesionario9',
              'email'     => 'concesionario9@gmail.com',
              'ubicacion'  => 'Av Casanova',
              'telefono'  => '12345678',
              'locate_id' => '5',
              'status_id' => '1',
          ]);
      Concessionaire::create(
          [
              'name'      => 'concesionario10',
              'email'     => 'concesionario10@gmail.com',
              'ubicacion'  => 'Av Baralt',
              'telefono'  => '12345678',
              'locate_id' => '5',
              'status_id' => '1',
          ]);
      Concessionaire::create(
          [
              'name'      => 'concesionario11',
              'email'     => 'concesionario11@gmail.com',
              'ubicacion'  => 'Av Panteon',
              'telefono'  => '12345678',
              'locate_id' => '6',
              'status_id' => '1',
          ]);
      Concessionaire::create(
          [
              'name'      => 'concesionario12',
              'email'     => 'concesionario12@gmail.com',
              'ubicacion'  => 'Av Paez',
              'telefono'  => '12345678',
              'locate_id' => '7',
              'status_id' => '1',
          ]);

    }
}
