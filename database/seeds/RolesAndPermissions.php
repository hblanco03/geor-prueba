<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesAndPermissions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        //Permission clientes
        Permission::create(['name' => 'clients index']);
        Permission::create(['name' => 'clients edit']);
        Permission::create(['name' => 'clients show']);
        Permission::create(['name' => 'clients create']);
        Permission::create(['name' => 'clients destroy']);

        Permission::create(['name' => 'concessionaires index']);
        Permission::create(['name' => 'concessionaires edit']);
        Permission::create(['name' => 'concessionaires show']);
        Permission::create(['name' => 'concessionaires create']);
        Permission::create(['name' => 'concessionaires destroy']);

        Permission::create(['name' => 'administration index']);
        Permission::create(['name' => 'reportes show']);
        Permission::create(['name' => 'bitacora show']);
        Permission::create(['name' => 'administration show']);



        $admin = Role::create(['name' => 'admin']);

        $admin->givePermissionTo(Permission::all());

        $lector = Role::create(['name' => 'lector']);

        $lector->givePermissionTo([
            'clients index',
            'clients show',
            'concessionaires index',
            'concessionaires show',
        ]);

        $creador = Role::create(['name' => 'creador']);

        $creador->givePermissionTo([
            'clients index',
            'clients show',
            'clients create',
            'concessionaires index',
            'concessionaires show',
            'concessionaires create',
        ]);

        $actualizador = Role::create(['name' => 'actualizador']);

        $actualizador->givePermissionTo([
            'clients index',
            'clients edit',
            'clients show',
            'clients create',
            'concessionaires index',
            'concessionaires edit',
            'concessionaires show',
            'concessionaires create',
        ]);

        $eliminador = Role::create(['name' => 'eliminador']);

        $eliminador->givePermissionTo([
            'clients index',
            'clients edit',
            'clients show',
            'clients create',
            'clients destroy',
            'concessionaires index',
            'concessionaires edit',
            'concessionaires show',
            'concessionaires create',
            'concessionaires destroy',
        ]);

    }
}
