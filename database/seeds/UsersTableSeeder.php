<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'name'     => 'admin',
            'email'    => 'admin@gmail.com',
            'password' => bcrypt('12345678'),
        ]);

        $admin->assignRole('admin');

        $lector = User::create([
            'name'     => 'lector',
            'email'    => 'lector@gmail.com',
            'password' => bcrypt('12345678'),
        ]);

        $lector->assignRole('lector');

        $creador = User::create([
            'name'     => 'creador',
            'email'    => 'creador@gmail.com',
            'password' => bcrypt('12345678'),
        ]);

        $creador->assignRole('creador');

        $actualizador = User::create([
            'name'     => 'actualizador',
            'email'    => 'actualizador@gmail.com',
            'password' => bcrypt('12345678'),
        ]);

        $actualizador->assignRole('actualizador');

        $eliminador = User::create([
            'name'     => 'eliminador',
            'email'    => 'eliminador@gmail.com',
            'password' => bcrypt('12345678'),
        ]);

        $eliminador->assignRole('eliminador');
    }
}
