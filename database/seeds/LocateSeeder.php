<?php

use App\Locate;
use Illuminate\Database\Seeder;

class LocateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Locate::create([

            'nombre'    => 'Caracas Las Mercedes',

        ]);
        Locate::create([

            'nombre'    => 'Miranda Los Teques',

        ]);
        Locate::create([

            'nombre'    => 'Miranda Higuerote',

        ]);
        Locate::create([

            'nombre'    => 'Trujillo Barbacoa',

        ]);


        Locate::create([

            'nombre'    => 'Merida El vigia',

        ]);
        Locate::create([

            'nombre'    => 'Margarita La asuncion',
          
        ]);
        Locate::create([

            'nombre'    => 'Margarita Juan Griego',
        ]);
    }
}
