<?php

use App\Client;
use Illuminate\Database\Seeder;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Client::create(
            [
                'name'      => 'Pedro Escobar',
                'dni'       => '123456789',
                'email'     => 'pescobar@gmail.com',
                'telefono'  => '12345678',
                'concessionaire_id' => '1',
                'status_id' => '1',
                'user_id'   => '1',
            ]);
        Client::create([
            'name'      => 'Daniel Barreto',
            'dni'       => '123456781',
            'email'     => 'daniel@gmail.com',
            'telefono'  => '12345678',
            'concessionaire_id' => '1',
            'status_id' => '1',
            'user_id'   => '1',
        ]);
        Client::create([
            'name'      => 'Elianhers Blanco',
            'dni'       => '123456782',
            'email'     => 'elianhers@gmail.com',
            'telefono'  => '12345678',
            'concessionaire_id' => '2',
            'status_id' => '1',
            'user_id'   => '1',
        ]);
        Client::create([
            'name'      => 'Angel Eduardo',
            'dni'       => '123456783',
            'email'     => 'angel@gmail.com',
            'telefono'  => '12345678',
            'concessionaire_id' => '2',
            'status_id' => '1',
            'user_id'   => '1',
        ]);
        Client::create(
            [
                'name'      => 'Javier Escobar',
                'dni'       => '123456789',
                'email'     => 'pescobar@gmail.com',
                'telefono'  => '12345678',
                'concessionaire_id' => '3',
                'status_id' => '1',
                'user_id'   => '1',
            ]);
        Client::create([
            'name'      => 'Jose Barreto',
            'dni'       => '123456781',
            'email'     => 'daniel@gmail.com',
            'telefono'  => '12345678',
            'concessionaire_id' => '3',
            'status_id' => '1',
            'user_id'   => '1',
        ]);
        Client::create([
            'name'      => 'Luis Blanco',
            'dni'       => '123456782',
            'email'     => 'elianhers@gmail.com',
            'telefono'  => '12345678',
            'concessionaire_id' => '4',
            'status_id' => '1',
            'user_id'   => '1',
        ]);
        Client::create([
            'name'      => 'Antonio Eduardo',
            'dni'       => '123456783',
            'email'     => 'angel@gmail.com',
            'telefono'  => '12345678',
            'concessionaire_id' => '4',
            'status_id' => '1',
            'user_id'   => '1',
        ]);
        Client::create(
            [
                'name'      => 'Penelope Escobar',
                'dni'       => '123456789',
                'email'     => 'pescobar@gmail.com',
                'telefono'  => '12345678',
                'concessionaire_id' => '5',
                'status_id' => '1',
                'user_id'   => '1',
            ]);
        Client::create([
            'name'      => 'Dainner Barreto',
            'dni'       => '123456781',
            'email'     => 'daniel@gmail.com',
            'telefono'  => '12345678',
            'concessionaire_id' => '5',
            'status_id' => '1',
            'user_id'   => '1',
        ]);
        Client::create([
            'name'      => 'Elian Blanco',
            'dni'       => '123456782',
            'email'     => 'elianhers@gmail.com',
            'telefono'  => '12345678',
            'concessionaire_id' => '6',
            'status_id' => '1',
            'user_id'   => '1',
        ]);
        Client::create([
            'name'      => 'Andres Eduardo',
            'dni'       => '123456783',
            'email'     => 'angel@gmail.com',
            'telefono'  => '12345678',
            'concessionaire_id' => '6',
            'status_id' => '1',
            'user_id'   => '1',
        ]);
        Client::create([
            'name'      => 'Brenda',
            'dni'       => '123456783',
            'email'     => 'angel@gmail.com',
            'telefono'  => '12345678',
            'concessionaire_id' => '7',
            'status_id' => '1',
            'user_id'   => '1',
        ]);
        Client::create([
            'name'      => 'Luz',
            'dni'       => '123456783',
            'email'     => 'angel@gmail.com',
            'telefono'  => '12345678',
            'concessionaire_id' => '8',
            'status_id' => '1',
            'user_id'   => '1',
        ]);
        Client::create([
            'name'      => 'Denisse',
            'dni'       => '123456783',
            'email'     => 'angel@gmail.com',
            'telefono'  => '12345678',
            'concessionaire_id' => '9',
            'status_id' => '1',
            'user_id'   => '1',
        ]);
        Client::create([
            'name'      => 'Abraham',
            'dni'       => '123456783',
            'email'     => 'angel@gmail.com',
            'telefono'  => '12345678',
            'concessionaire_id' => '10',
            'status_id' => '1',
            'user_id'   => '1',
        ]);
        Client::create([
            'name'      => 'Hector',
            'dni'       => '123456783',
            'email'     => 'angel@gmail.com',
            'telefono'  => '12345678',
            'concessionaire_id' => '11',
            'status_id' => '1',
            'user_id'   => '1',
        ]);
        Client::create([
            'name'      => 'Julia',
            'dni'       => '123456783',
            'email'     => 'angel@gmail.com',
            'telefono'  => '12345678',
            'concessionaire_id' => '12',
            'status_id' => '1',
            'user_id'   => '1',
        ]);

    }
}
