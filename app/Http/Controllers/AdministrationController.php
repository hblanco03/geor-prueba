<?php

namespace App\Http\Controllers;

use IlluminatecHttp\Request;
use App\Roles;
use App\User;
use App\Auditory;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;

class AdministrationController extends Controller
{
    public function index()
    {
      return view('welcomelogin');
    }
    public function init()
    {
      $actions=Auditory::join('users', 'users.id', '=', 'auditories.user_id')
      ->select('auditories.action', 'users.name','auditories.created_at')
      ->get();
      return $actions;
    }
    public function getRoles()
    {
      $roles = Roles::all();
      $permi = Permission::all();
      $permissions = DB::table('role_has_permissions')
      ->join('permissions', 'permissions.id', '=', 'role_has_permissions.permission_id')
      ->select('permission_id','role_id','name')->get();
      $users = User::all();

      return compact('roles','users','permissions','permi');
    }
    public function destroy($id)
    {
      DB::table('role_has_permissions')->where('role_id',$id)->delete();

      Roles::find($id)->delete();
      $mensaje = 'Eliminó el rol '.$id;
      DB::table('auditories')->insert([
          'user_id' => Auth::user()->id,
          'action'  => $mensaje,
          'created_at'  => date('Y-m-d H:m:s')
      ]);


    }
    public function infRol($id)
    {
      $p_on = DB::table('role_has_permissions')
      ->join('permissions', 'permissions.id', '=', 'role_has_permissions.permission_id')
      ->select('permissions.id','permissions.name')->where('role_id',$id)->get();

      $p_off = DB::select("select id, name from clientes.permissions WHERE id not in (SELECT permission_id FROM clientes.role_has_permissions
      where role_id='$id')");


      return compact('p_on','p_off');

    }
}
