<?php

namespace App\Http\Controllers;

use App\Concessionaire;
use App\Locate;

use App\Auditory;
use App\User;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class ConcessionaireController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('welcomelogin');
    }

    public function ConcessionaireInit()
    {
        //
        $concessionaires = Concessionaire::all()->where('status_id',1);
        $locates = locate::all();

        return compact('concessionaires','locates');
    }

    public function getLocate()
    {
        //
        $locates = locate::all();
        return $locates;
    }
    public function infoConcessionaire()
    {
        //
        $concessionaires = Concessionaire::all()->where('status_id',1);

        return $concessionaires;
    }


    public function infConc($id)
    {
      $concessionaire = Concessionaire::findOrFail($id);
      return $concessionaire;
    }
    public function store_(Request $request)
    {
      $id = $request->id;
      $email = $request->email;
      $name = $request->name;
      $ubicacion = $request->ubicacion;
      $telefono = $request->telefono;
      $locate_id = $request->locate_id;
      $status_id = 1;

        Concessionaire::updateOrCreate(
        ['id' => $id],
        [
        'email' => $email,
        'name' => $name,
        'ubicacion' => $ubicacion,
        'telefono' => $telefono,
        'locate_id' => $locate_id,
        'status_id' => 1,
        ]
      );
      if ($id=0) {
        $mensaje='Creó el concesionario '.$name;
      }else{
        $mensaje='Editó el concesionario '.$name;
      }
      DB::table('auditories')->insert([
          'user_id' => Auth::user()->id,
          'action'  => $mensaje,
          'created_at'  => date('Y-m-d H:m:s')
      ]);


      return;

    }

    public function update(Request $request, Concessionaire $concessionaire)
    {
        //
    }


    public function destroy($id)
    {
      $concessionaire = Concessionaire::find($id);
      $concessionaire->status_id = 2;
      $concessionaire->save();
      $mensaje = 'Eliminó el concesionario '.$id;

      DB::table('auditories')->insert([
          'user_id' => Auth::user()->id,
          'action'  => $mensaje,
          'created_at'  => date('Y-m-d H:m:s')
      ]);
    }
}
