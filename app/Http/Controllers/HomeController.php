<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Client;
use App\Concessionaire;
use App\Locate;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        return view('welcomelogin');
    }
    public function homeInit()
    {
      $role_id    = Auth::user()->roles->implode('id', ',');
      $permissions = DB::table('role_has_permissions')->select('permission_id')->where('role_id', $role_id )->get();
      return $permissions;
    }

    public function saludo()
    {

        $user   = Auth::user();
        $mensaje ='El usuario '.Auth::user()->id.' inició sesión';
        DB::table('auditories')->insert([
            'user_id' => Auth::user()->id,
            'action'  => $mensaje,
            'created_at'  => date('Y-m-d H:m:s')
        ]);
        $locates_count = Locate::count();
        $rol    = $user->roles->implode('name', ',');
        $saludo = 'Bienvenido ' .Auth::user()->name;
        $clients_on = Client::where('status_id',1)->count();

        $clients_off = Client::where('status_id',2)->count();
        $concessionaires = Concessionaire::where('status_id',1)->count();

        $conces = Concessionaire::all();
        $locates = Locate::all();
        return compact('saludo','clients_on','clients_off','concessionaires','locates_count','conces','locates');
    }
}
