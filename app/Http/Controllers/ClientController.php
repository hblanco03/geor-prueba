<?php

namespace App\Http\Controllers;

use App\Client;
use App\Concessionaire;
use App\Auditory;
use App\User;
use App\Status;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('welcomelogin');
    }

    public function ClientInit()
    {
        //
        $clients = Client::all()->where('status_id',1);
        $concessionaires = Concessionaire::all();
        $statuses = Status::all();
        $users = User::all();

        return compact('clients','concessionaires','statuses','users');
    }

    public function infClient($id)
    {
      $clients = Client::findOrFail($id);
      return $clients;
    }

    public function store_(Request $request)
    {
      $id = $request->id;
      $name = $request->name;
      $dni = $request->dni;
      $email = $request->email;
      $telefono = $request->telefono;
      $concessionaire_id = $request->concessionaire_id;
      $status_id = 1;
      $user_id   = Auth::user()->id;


        Client::updateOrCreate(
        ['id' => $id],
        [
        'email' => $email,
        'name' => $name,
        'dni' => $dni,
        'telefono' => $telefono,
        'concessionaire_id' => $concessionaire_id,
        'status_id' => 1,
        'user_id' => $user_id,
        ]
      );
      if ($id=0) {
        $mensaje='Creó el cliente '.$name;
      }else{
        $mensaje='Editó el cliente '.$name;
      }
      DB::table('auditories')->insert([
          'user_id' => Auth::user()->id,
          'action'  => $mensaje,
          'created_at'  => date('Y-m-d H:m:s')
      ]);

      return;

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $client = Client::find($id);
      $client->status_id = 2;
      $client->save();
      $mensaje = 'Eliminó el cliente '.$id;

      DB::table('auditories')->insert([
          'user_id' => Auth::user()->id,
          'action'  => $mensaje,
          'created_at'  => date('Y-m-d H:m:s')
      ]);

    }
}
