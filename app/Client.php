<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = ['name', 'dni', 'email', 'telefono', 'concessionaire_id', 'status_id', 'user_id'];
}
