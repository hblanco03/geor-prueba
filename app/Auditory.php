<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Auditory extends Model
{
    protected $fillable = ['user_id', 'action','created_at','updated_at'];
}
