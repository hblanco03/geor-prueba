<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Concessionaire extends Model
{
    protected $fillable = ['name', 'email', 'ubicacion', 'telefono', 'locate_id','status_id'];
}
