import Vue from 'vue';
import Router from 'vue-router';
import clients from './views/clients.vue';
import home from './views/home.vue';
import concessionaires from './views/concessionaires.vue';
import reportes from './views/reportes.vue';
import administracion from './views/administracion.vue';
import bitacora from './views/bitacora.vue';
Vue.use(Router)
export default new Router({
    routes: [{
        path: '/home',
        name: 'home',
        component: home,
    }, {
        path: '/clients',
        name: 'clients',
        component: clients
    }, {
        path: '/concessionaires',
        name: 'concessionaires',
        component: concessionaires
    }, {
        path: '/reportes',
        name: 'reportes',
        component: reportes
    },{
        path: '/administracion',
        name: 'administracion',
        component: administracion
    },{
        path: '/bitacora',
        name: 'bitacora',
        component: bitacora
    }],
    mode: 'history'
})
