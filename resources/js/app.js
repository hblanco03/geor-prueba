require('./bootstrap');
window.Vue = require('vue');
Vue.component('app', require('./components/AppComponent.vue').default);
Vue.component('home', require('./views/home.vue').default);
Vue.component('clients', require('./views/clients.vue').default);
Vue.component('concessionaires', require('./views/concessionaires.vue').default);
Vue.component('reportes', require('./views/reportes.vue').default);
import router from './routes';
const app = new Vue({
    el: '#app',
    router,
});