<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Geor | @yield('prueba','prueba-pagina')</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="icon" href="{{asset('assets/AdminMaster/favicon.ico')}}" type="image/x-icon" />

        <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,600,700,800" rel="stylesheet">
        <link href="{{asset('assets/cssDataTables.css')}}" rel="stylesheet">
        <link href="{{asset('assets/dataTables.min.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('assets/AdminMaster/plugins/bootstrap/dist/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/AdminMaster/plugins/fontawesome-free/css/all.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/AdminMaster/plugins/ionicons/dist/css/ionicons.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/AdminMaster/plugins/icon-kit/dist/css/iconkit.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/AdminMaster/plugins/perfect-scrollbar/css/perfect-scrollbar.css')}}">
        <link rel="stylesheet" href="{{asset('assets/AdminMaster/plugins/tempusdominus-bootstrap-4/build/css/tempusdominus-bootstrap-4.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/AdminMaster/dist/css/theme.min.css')}}">

        <link rel="stylesheet" href="{{asset('ccs/app.css')}}">



        <script src="{{asset('assets/AdminMaster/src/js/vendor/modernizr-2.8.3.min.js')}}"></script>


        @yield('styles')
    </head>

    <body>
        <div class="wrapper">
            @include('AdminMaster/header')
            <div class="page-wrap">

                <div id='app'>
                    <app/>
                </div>


             @include('AdminMaster/footer')
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script>window.jQuery || document.write("<script src="{{asset('assets/AdminMaster/src/js/vendor/jquery-3.3.1.min.js')}}"><\/script>")</script>
        <script src="{{asset('assets/AdminMaster/plugins/popper.js/dist/umd/popper.min.js')}}"></script>
        <script src="{{asset('assets/AdminMaster/plugins/bootstrap/dist/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('assets/AdminMaster/dist/js/theme.min.js')}}"></script>
        <script src="{{asset('assets/AdminMaster/plugins/perfect-scrollbar/dist/perfect-scrollbar.min.js')}}"></script>
        <script src="{{asset('assets/AdminMaster/plugins/screenfull/dist/screenfull.js')}}"></script>
        <script src="{{asset('assets/AdminMaster/plugins/moment/moment.js')}}"></script>
        <script src="{{asset('assets/AdminMaster/plugins/tempusdominus-bootstrap-4/build/js/tempusdominus-bootstrap-4.min.js')}}"></script>



        @yield('scripts')
        <script src="{{asset('js/app.js')}}"></script>

        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
        <script src="{{asset('js/queryDataTable.js')}}"></script>
        <script src="{{asset('js/dataTable.js')}}"></script>

    </body>
</html>
