<footer class="footer">
    <div class="w-100 clearfix">
        <span class="text-center text-sm-left d-md-inline-block">Copyright © 2020</span>
        <span class="float-none float-sm-right mt-1 mt-sm-0 text-center">Elaborado por <i class="fa fa-heart text-danger"></i> <a href="http://lavalite.org/" class="text-dark" target="_blank">Hillary Blanco</a></span>
    </div>
</footer>
