<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
//rutas que acceden todos
Route::get('/', function () {return view('welcome');});
Auth::routes();
//rutas de consumo
Route::get('home_init', 'HomeController@homeInit')->name('home_init');
Route::get('saludo', 'HomeController@saludo')->name('saludo');
Route::get('clientinit', 'ClientController@ClientInit')->name('clientinit');
Route::get('concessionaireinit', 'ConcessionaireController@ConcessionaireInit')->name('concessionaireinit');
Route::get('reportesinit', 'ReporteController@ReportesInit')->name('reportesinit');
Route::get('reportesconsul/{id}', 'ReporteController@ReportesConsul')->name('reportesconsul');
Route::get('getRoles', 'AdministrationController@getRoles')->name('getRoles');
Route::get('bitacorainit', 'AdministrationController@init')->name('bitacorainit');

//rutas de vistas generales
Route::get('home', 'HomeController@index')->name('home');
Route::resource('clients', 'ClientController');
Route::resource('concessionaires', 'ConcessionaireController');


//rutas para editar y crear clientes
Route::group(['middleware' => ['permission: clients create|clients edit']], function () {
  Route::post('save_client', 'ClientController@store_')->name('save_client');
  Route::get('info_client/{id}', 'ClientController@infClient')->name('info_client');
});

//rutas para editar y crear concesionarios
Route::group(['middleware' => ['permission:concessionaires edit|concessionaires create']], function () {
  Route::post('save_conce', 'ConcessionaireController@store_')->name('save_conce');
  Route::get('info_conce/{id}', 'ConcessionaireController@infConc')->name('info_conce');
});

//vistas solo root
Route::group(['middleware' => ['permission:reportes show|bitacora show|administartion show']], function () {
  Route::resource('reportes', 'ReporteController');
  Route::get('administracion', 'AdministrationController@index')->name('administracion');
  Route::delete('administraciond/{id}', 'AdministrationController@destroy')->name('administraciond');
  Route::get('bitacora', 'AdministrationController@index')->name('bitacora');
  Route::post('save_rol', 'AdministrationController@store_')->name('save_rol');
  Route::get('info_rol/{id}', 'AdministrationController@infRol')->name('info_rol');
});
